﻿<font size =6>**操作系统原理 实验五**</font>

## 个人信息

（此部分需补充完整

【院系】计算机学院

【专业】计算机科学与技术

【学号】22336312

【姓名】郑陈楠

## 实验题目

内核线程

## 实验目的

1. 学习C语言的可变参数机制的实现方法，实现可变参数机制，以及实现一个较为简单的printf函数。
2. 实现内核线程的实现。
3. 重点理解`asm_switch_thread`是如何实现线程切换的，体会操作系统实现并发执行的原理。
4. 实现基于时钟中断的时间片轮转(RR)调度算法，并实现一个线程调度算法。

## 实验要求

1. 实现了可变参数机制及printf函数。
2. 自行实现PCB，实现线程。
3. 了解线程调度，并实现一个调度算法。
4. 撰写实验报告。



## 实验过程

Assignment1 printf的实现
在这个实验中我模仿"%x"的方法，将一个浮点数分为整数部分和小数部分以及小数点，分别存入到缓冲区中，当成字符串分别打印出来，这样就可以用itos这个函数直接打印出来：
![](1.png)
Assignment2 线程的实现
在这个任务中PCB的各个属性已经定义好，所以只需要学会怎么使用这些属性。
修改setup_up.cpp函数，利用PCB的priority将每一个进程的优先级打印出来：
![](2.png)
如图，0、1、2号进程分别对应的优先级为1、2、3
Assignment3 线程调度切换的秘密
操作系统的线程能够并发执行的秘密在于我们需要中断线程的执行，保存当前线程的状态，然后调度下一个线程上处理机，最后使被调度上处理机的线程从之前被中断点处恢复执行。
在这个实验中我用gdb跟踪了
c_time_interrupt_handler()、schedule()这两个函数
![](3.png)
![](4.png)
可以看到ticks在执行之后会逐渐递减直到变为0，这是因为每一次执行都代表消耗了一个时间片。
![](5.png)
当时间片变为0时跳转到第二个断点，也就是schedule()函数这里发生调度，此时我们再次查看pid，ticks，ticksPassedBy这三个属性：
![](6.png)
可以看到在发生调度前pid=0的进程消耗了十个时间片，紧接着我们继续执行看看发生了什么：
![](7.png)
可以看到时间片消耗结束后我们发生了调度，切换到pid=1的进程。
接着我们查看asm_switch_thread中的寄存器值：
![](8.png)
![](9.png)
这些信息表示了当前线程的状态，当ret返回后再次查看寄存器信息，表示了新线程的状态。
Assignment4 调度算法的实现
在这个任务中我们修改调度算法，从原来的时间片轮换(RR)改为先来先服务调度算法：
首先将每次调用线程后得出挂起语句注释：
![](11.png)
然后修改program.exit()函数，其逻辑是当有线程终止且就绪队列非空时，进行一次调度，否则挂起：
![](12.png)
接着修改中断处理函数，RR算法可能需要调度，但是先来先服务不需要，所以不做任何操作，直接注释掉代码：
![](13.png)
至此就修改结束，使用make&&make run命令执行查看结果：
![](10.png)
可以看到三个线程按照存入到就绪队列的顺序执行




## 实验总结

通过完成这个实验，本人获得了以下几个方面的总结与体会：
◆ 深入理解线程调度：实验加深了本人对操作系统线程调度机制的理解，包括线程状态的转换、时间片的概念以及调度算法的工作原理。
◆ 掌握 GDB 调试技巧：通过使用 GDB 来跟踪线程的执行和上下文切换，本人学会了如何设置断点、单步执行、查看和修改寄存器内容以及程序计数器（PC）。
◆ 直观观察上下文切换：实验让本人亲眼见证了上下文切换的过程，包括栈的变化、寄存器的保存和恢复，以及程序如何在中断后从相同的状态恢复执行。
◆ 理论与实践相结合：通过编写线程函数并观察它们的执行，本人能够将理论知识应用到实践中，加深了对操作系统工作原理的认识。

## 参考文献

（如有要列出，包括网上资源）